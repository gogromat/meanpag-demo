var _ = require('lodash');

module.exports = function (mongoose) {

    var mongoObjectId = mongoose.Types.ObjectId;

    var UserSchema = new mongoose.Schema({
        firstName    : { type: String, required: true },
        lastName     : { type: String, required: true },
        description  : { type: String },
        age          : { type: Number, min: 0, max: 200 },
        createdAt    : { type: Date, default: Date.now },
        address      : {
            zipCode     : { type: String },
            city        : { type: String },
            streetName  : { type: String },
            state       : { type: String }
        },
        phones: [
            { phoneNumber: { type: String } }
        ]
    });

    var Schemas = {
        Users : mongoose.model('Users', UserSchema)
    };

    return {
        Users : Schemas.Users
    };

};