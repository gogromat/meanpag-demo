var fs = require('fs'),
    Faker = require('Faker');


var randomMinMax = function (min, max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    },
    randomAge = function () {
        return randomMinMax(1, 200);
    };

var fakeUsers = [];

for (var i = 0; i < 20000; i ++) {

    var phones = [];

    for (var p = 0; p <= randomMinMax(0, 3); p ++) {
        phones.push({ phoneNumber: Faker.PhoneNumber.phoneNumber() });
    }

    var fakeUser = {
        firstName : Faker.Name.firstName(),
        lastName  : Faker.Name.lastName(),
        description  : Faker.Lorem.paragraph(),
        age          : randomAge(),
        createdAt    : Date.now(),
        address      : {
            zipCode     : Faker.Address.zipCode(),
            city        : Faker.Address.city(),
            streetName  : Faker.Address.streetName(),
            state       : Faker.Address.usState()
        },
        phones: phones
    };

    fakeUsers.push(fakeUser);
}

fs.writeFile('./seeds/users.json', JSON.stringify(fakeUsers), function (err) {
    if (err) {
        console.log("There was an error saving users");
    } else {
        console.log("Success: users.json generated!");
    }
});