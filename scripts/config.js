config = {};

config.DEV_STATE = "dev";
config.PROD_STATE = "prod";

config.dev = {
    state       : config.DEV_STATE,
    PORT        : '3000',
    IP          : '127.0.0.1',
    MONGO_URL   : 'mongodb://127.0.0.1/paginate',
    CACHE       : false,
    superuser   : "gogromat@gmail.com",
    basePort    : 10000
};

config.prod = {
//    state       : config.PROD_STATE,
//    PORT      	: process.env.OPENSHIFT_NODEJS_PORT,
//    IP          : process.env.OPENSHIFT_NODEJS_IP,
//    CACHE       : true,
//    superuser 	: "gogromat@gmail.com",
//    basePort    : 15000
};

var state = 'dev';
module.exports = (state === 'prod') ? config.prod : config.dev;