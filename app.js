var express              = require('express')
    // json, form
  , bodyParser           = require('body-parser')
    // multi-part (files)
  , busboy               = require('connect-busboy')
  , methodOverride       = require('method-override')
  , cookieParser         = require('cookie-parser')
  , router               = express.Router()
  , favicon              = require('static-favicon')
  , session              = require('express-session')
  , morgan               = require('morgan')
  , errorHandler         = require('errorhandler')
  , _                    = require('lodash')
  , async                = require('async')

  , flash                = require('connect-flash')

  , mongoose             = require('mongoose')

  , schemas              = require('./scripts/schemas.js')(mongoose)
  , Users                = schemas.Users

  // Configuration File
  , config               = require('./scripts/config')
  , mongooseConnection   = mongoose.connect(config.MONGO_URL)

  , jade                 = require('jade')
  , http                 = require('http')
  , path                 = require('path')
  , fs                   = require('fs')

  // Issue server-side requests
  , request              = require('request')
  // APP
  , app                  = express()

  , Paginator            = require('meanpag').Paginator
  ;

// Set base port depending on the server we run on
//portfinder.basePort = config.basePort;

app.engine('jade', jade.__express);

app.set('port', config.PORT);
app.set('id',   config.IP  );
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('view cache', config.cache);

// Strict Trailing Slash requests ex. users/
//app.set('strict routing', true);
// Local user Time
//app.set('local messaging', true);

app.use(favicon());
app.use(morgan());

/* Middleware - order is very important */
// READ MORE: senchalabs.org/connect

// Static folder (stylesheets, javascript, images, etc)
// bower and project files
app.use(express.static(path.join(__dirname, 'public')));
// npm modules
app.use(express.static(path.join(__dirname, 'node_modules')));

// makes sure to allow to use meanpag library resource on client :)
app.get('/meanpag.js', function(req, res) {
    res.sendfile('./node_modules/meanpag/dist/paginator.js');
});

app.use("/", bodyParser());

// Sessions are implemented using cookies, load Cookie Parter First
app.use(cookieParser() );

// Can convert POST/GET requests into PUT/DELETE
// Must come after the bodyparser so that POST Request's body __method
// can be parsed from it
app.use(methodOverride());

// Must start after cookieParser
app.use(session({ secret: 'MY_SUPER_SECRET_KEY_' }));

app.use(flash());

app.use(function (err, req, res, next) {
    res.status(err.status || 404);
    res.send(err.message);
});

app.use(function(req, res, next) {
    req.getPath = function () {
        return req.originalUrl ? req.originalUrl.substr(1) : "";
    };
    req.getUrl = function() {
        return req.protocol + "://" + req.get('host') + (req.getPath() ? "/" + req.getPath() : "");
    };
    req.getBaseUrl = function () {
        return req.protocol + "://" + config.IP + ":" + config.PORT;
    };
    req.getFullUrl = function (url) {
        return req.getBaseUrl() + url;
    };
    req.getRawCookie = function () {
        return req.headers.cookie;
    };
    req.getRandom = function () {
        return (Math.random().toString(36)).slice(2, 17);
    };
    return next();
});


if (config.state == config.DEV_STATE) {
    app.use(errorHandler());
}

router.get("/", function (req, res) {
    res.render("layout");
});

// getUsers,
router.post("/users", setupPaginationFilter, getUsers, function (req, res, next) {
    return sendSuccessResponse({
        filter: res.locals.paginator.getFilter(),
        users : res.locals.users
    }, res, next);
});

function setupPaginationFilter (req, res, next) {
    res.locals.paginator = new Paginator().setFilter(req.body.filter);
    return next();
}

function getUsers (req, res, next) {

    res.locals.paginator.filterMongooseCollection(
        Users,
        function errorCallback (err) {
            return sendErrorResponse(err, res, next);
        },
        function successCallback (data) {
            res.locals.users = data;
            return next();
        });
}

function requestedWithWrongType (req, res, next) {
    var errorMessage = "Wrong Request Type.";
    req.flash("error", errorMessage + " Page only accepts " + getRequestAcceptedTypes(req));
    res.send("error " + errorMessage);
}

function getRequestAcceptedTypes (req) {
    var acceptedTypes = [];
    req.accepted.forEach(function (type) {
        acceptedTypes.push(type.value);
    });
    return acceptedTypes.join(", ");
}

function sendErrorResponse (err, res, next) {
    var errorMessage = getErrorMessage(err);

    logger.logError(errorMessage);

    if (res) return res.json({
        success : false,
        error   : errorMessage
    });
    return next && next(err);
}

function sendSuccessResponse (params, res, next) {

    if (params && params.json && typeof params.json === "function") {
        // checks if it is Express's response object wrapper,
        // changes the arguments
        next = res;
        res = params;
        params = {};
    }

    var responseData = { success: true };
    _.extend(responseData, params);

    if (res) return res.json(responseData);
    return next && next();
}

function getErrorMessage (err) {
    var errorMessage = "";
    if (_.isArray(err)) {
        err.forEach(function (e) {
            errorMessage += getErrorString(e);
        });
    } else {
        errorMessage += getErrorString(err);
    }
    return errorMessage;
}

function getErrorString (err) {
    if (err && err.toJSON)   return err.toJSON();
    if (err && err.toString) return err.toString();
    if (err)                 return err;
    return "Undefined error";
}

app.use("/", router);

app.listen(config.PORT, config.IP);

app.use(function (req, res, next) {

    var query = "";

    if (req.getPath()) {
         query = "?redirectedFrom=" + req.getPath();
    }

    // 404 error
    if (req.accepts(["html"]))
        return res.redirect("/" + query);
    if (req.accepts(["json"]))
        return sendErrorResponse("Wrong page", res, next);

    return requestedWithWrongType(req, res, next);
});

console.log('Express server listening on ' + config.IP + ":" + config.PORT);