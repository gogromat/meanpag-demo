;$(document).ready(function() {

    var usStates = [
        { name: 'ALABAMA', abbreviation: 'AL'},
        { name: 'ALASKA', abbreviation: 'AK'},
        { name: 'AMERICAN SAMOA', abbreviation: 'AS'},
        { name: 'ARIZONA', abbreviation: 'AZ'},
        { name: 'ARKANSAS', abbreviation: 'AR'},
        { name: 'CALIFORNIA', abbreviation: 'CA'},
        { name: 'COLORADO', abbreviation: 'CO'},
        { name: 'CONNECTICUT', abbreviation: 'CT'},
        { name: 'DELAWARE', abbreviation: 'DE'},
        { name: 'DISTRICT OF COLUMBIA', abbreviation: 'DC'},
        { name: 'FEDERATED STATES OF MICRONESIA', abbreviation: 'FM'},
        { name: 'FLORIDA', abbreviation: 'FL'},
        { name: 'GEORGIA', abbreviation: 'GA'},
        { name: 'GUAM', abbreviation: 'GU'},
        { name: 'HAWAII', abbreviation: 'HI'},
        { name: 'IDAHO', abbreviation: 'ID'},
        { name: 'ILLINOIS', abbreviation: 'IL'},
        { name: 'INDIANA', abbreviation: 'IN'},
        { name: 'IOWA', abbreviation: 'IA'},
        { name: 'KANSAS', abbreviation: 'KS'},
        { name: 'KENTUCKY', abbreviation: 'KY'},
        { name: 'LOUISIANA', abbreviation: 'LA'},
        { name: 'MAINE', abbreviation: 'ME'},
        { name: 'MARSHALL ISLANDS', abbreviation: 'MH'},
        { name: 'MARYLAND', abbreviation: 'MD'},
        { name: 'MASSACHUSETTS', abbreviation: 'MA'},
        { name: 'MICHIGAN', abbreviation: 'MI'},
        { name: 'MINNESOTA', abbreviation: 'MN'},
        { name: 'MISSISSIPPI', abbreviation: 'MS'},
        { name: 'MISSOURI', abbreviation: 'MO'},
        { name: 'MONTANA', abbreviation: 'MT'},
        { name: 'NEBRASKA', abbreviation: 'NE'},
        { name: 'NEVADA', abbreviation: 'NV'},
        { name: 'NEW HAMPSHIRE', abbreviation: 'NH'},
        { name: 'NEW JERSEY', abbreviation: 'NJ'},
        { name: 'NEW MEXICO', abbreviation: 'NM'},
        { name: 'NEW YORK', abbreviation: 'NY'},
        { name: 'NORTH CAROLINA', abbreviation: 'NC'},
        { name: 'NORTH DAKOTA', abbreviation: 'ND'},
        { name: 'NORTHERN MARIANA ISLANDS', abbreviation: 'MP'},
        { name: 'OHIO', abbreviation: 'OH'},
        { name: 'OKLAHOMA', abbreviation: 'OK'},
        { name: 'OREGON', abbreviation: 'OR'},
        { name: 'PALAU', abbreviation: 'PW'},
        { name: 'PENNSYLVANIA', abbreviation: 'PA'},
        { name: 'PUERTO RICO', abbreviation: 'PR'},
        { name: 'RHODE ISLAND', abbreviation: 'RI'},
        { name: 'SOUTH CAROLINA', abbreviation: 'SC'},
        { name: 'SOUTH DAKOTA', abbreviation: 'SD'},
        { name: 'TENNESSEE', abbreviation: 'TN'},
        { name: 'TEXAS', abbreviation: 'TX'},
        { name: 'UTAH', abbreviation: 'UT'},
        { name: 'VERMONT', abbreviation: 'VT'},
        { name: 'VIRGIN ISLANDS', abbreviation: 'VI'},
        { name: 'VIRGINIA', abbreviation: 'VA'},
        { name: 'WASHINGTON', abbreviation: 'WA'},
        { name: 'WEST VIRGINIA', abbreviation: 'WV'},
        { name: 'WISCONSIN', abbreviation: 'WI'},
        { name: 'WYOMING', abbreviation: 'WY' }
    ];

    // Access the MongoPag library from global namespace
    var MEANPag = window.MEANPag,
        paginator = new MEANPag.Paginator(),
        QueryBuilder = MEANPag.QueryBuilder;

    buildStates();
    fetchUsers();

    function fetchUsers () {

        getPageSearchOptions();

        $.ajax({
            method: 'POST',
            url : '/users',
            data: { filter: paginator.getFilter() },
            success: function (data) {
                if (data.success == true) {
                    console.log(data.filter, "<<< BACK");
                    paginator.setFilter(data.filter).clearFind();
                    setPagesList(data.filter);
                    setUsersList(data.users);
                }
            },
            dataType: 'json'
        });
    }

    function getPageSearchOptions () {
        // User names
        var name = $("#name").val();
        if (name) {
            console.log("Name:",name);
            paginator
                .or({'firstName': QueryBuilder.regex(name, "i")})
                .or({'lastName': QueryBuilder.regex(name, "i") });
        }

        var state = $(".states option:selected").val();
        if (state) {
            console.log("State:", state, $(".states option:selected"));
            paginator
                .and({'address.state': QueryBuilder.regex(state, "i") });
        }

        var zip = $("#zip").val();
        if (zip) {
            console.log("Zip:", (zip).toString(), QueryBuilder.regex((zip).toString()));
            paginator
                .findWhere({
                    "address.zipCode": QueryBuilder.regex((zip).toString(), "i")
                });

            console.log(
                paginator.getFilter(),
                JSON.stringify(paginator.getFilter())
            );
        }

        var age = $("#age").val();
        if (age) {
            console.log("Age:", age);
            paginator
                .and({'age': parseInt(age, 10) });
        }

        var phone = $("#phone").val();
        if (phone) {
            console.log("Phone:", phone);
            paginator
                .and({'phones.phoneNumber': QueryBuilder.in(phone) });
        }
    }

    function buildStates () {
        var stateSelect = "<select name='states' class='states'>";
        stateSelect += "<option value=''>All states</option>";
        for (var i = 0; i < usStates.length; i++) {
            stateSelect += "<option value='" + usStates[i].name + "'>" + usStates[i].abbreviation + "&nbsp;&nbsp;" + usStates[i].name + "</option>";
        }
        stateSelect += "</select>";
        $(".statesHolder").append(stateSelect);
    }

    function setUsersList (users) {
        var userTableItems = "";
        users.forEach(function (user) {
            var phoneStr = "";
            user.phones.forEach(function (p) {
                phoneStr += p.phoneNumber + "<br>";
            });
            userTableItems += "<tr>";
            userTableItems += "<td>" + user.firstName + "</td>";
            userTableItems += "<td>" + user.lastName + "</td>";
            userTableItems += "<td>" + user.description + "</td>";
            userTableItems += "<td>" + new Date(user.createdAt).toLocaleString() + "</td>";
            userTableItems +=
                "<td>" +
                user.address.streetName + "<br>" +
                user.address.city       + "<br>" +
                user.address.state      + "<br>" +
                "<b>" + user.address.zipCode + "</b>" +
                "</td>";
            userTableItems += "<td>" + phoneStr + "</td>";
            userTableItems += "<td>" + user._id + "</td>";
            userTableItems += "</tr>";
        });

        $("tbody.usersBody").html(userTableItems);
    }

    function setPagesList (filter) {
        var pagesLinks = "",
            totalPages = paginator.getTotalPages();

        $(".paginationFilter").html(JSON.stringify(filter));

        if (totalPages > 0) {
            for (var i = 1; i <= totalPages; i++) {
                pagesLinks += "<button class='pages' id='page" + i + "'>Page " + i + "</button>";
            }
            $(".paginationFilter").append(pagesLinks);
        }
    }


    $("body").on("click", '.pages', function (e) {
        // Set page number
        var pageNum = $(this).attr("id").substr(4);
        paginator.setPageNumber(pageNum);
        console.log("PAGE:", pageNum, paginator.getPageNumber());
        fetchUsers();
    });
    $("body").on("click", '#search', function (e) {
        fetchUsers();
    });
    $("body").on("change", '.states', function (e) {
        fetchUsers();
    });

});